"use strict";

const gulp = require("gulp");
const browsersync = require("browser-sync").create();
const inlineCss = require("gulp-inline-css");

function inline() {
  return gulp.src("./index.html")
      .pipe(inlineCss({
          applyStyleTags: false,
          removeStyleTags: false,
          applyLinkTags: true,
      }))
      .pipe(gulp.dest("./build"));
}


function browserSync(done){
  browsersync.init({
      server: {
          baseDir: "./build"
      },
      port: 3000
  });
  done();
}

function browserSyncReload(done){
  browsersync.reload();
  done();
}


function watchFiles(done) {
  gulp.watch(["*.html", "*.css"], gulp.series(inline, browserSyncReload));
  done();
}


gulp.task("inline", inline);
gulp.task("watch", gulp.parallel(watchFiles, browserSync));
